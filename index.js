function prueba(str) {
    let aux = str.replace(/\s/g,'') // elimino los espacios
    aux =  aux.toLowerCase() // esto lo googleé
    let isCapicua = aux.split('').reverse().join('') // hago del string un array, lo invierto y lo hago string
    return aux === isCapicua ? true : false // comparo los resultados
}

function isPalindrome(str) {
    //le puse throw pero no estoy muy segura (tuve que tocar los test para probar, los venía tocando desde hace 2 commit)
    let result=false
    try {
        if (typeof str=='string') {
            result=prueba(str)
        }
    }
    catch (err){
       throw err
    }
    return result
}

module.exports = isPalindrome